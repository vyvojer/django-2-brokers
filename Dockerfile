FROM python:3.6

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV WORKDIR /app

WORKDIR $WORKDIR

RUN mkdir -p $WORKDIR
COPY ./requirements.txt $WORKDIR
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY ./testapp $WORKDIR