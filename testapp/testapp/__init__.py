from testapp.celery_rabbit import app as celery_app

__all__ = ["celery_app",]