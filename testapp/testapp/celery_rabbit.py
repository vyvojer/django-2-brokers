import os

from celery import Celery

#app = Celery('app_2', broker='pyamqp://rabbitmq:rabbitmq@localhost:5672//')
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "testapp.settings")
app = Celery('first', broker="pyamqp://rabbitmq:rabbitmq@rabbitmq:5672//", include=["test.tasks.rabbit"])


# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object("django.conf:settings")
#app.autodiscover_tasks([])
