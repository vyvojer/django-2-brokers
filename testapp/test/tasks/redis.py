from testapp.celery_redis import app
from test.models import UserWebsite


@app.task
def task_add_website_via_redis_1():
    print("Adding via redis. queue 1")
    UserWebsite.objects.create(url="queue-1.redis.com")

@app.task
def task_add_website_via_redis_2():
    print("Adding via redis. queue 2")
    UserWebsite.objects.create(url="queue-2.redis.com")
