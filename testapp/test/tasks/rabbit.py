from testapp.celery_rabbit import app
from celery import shared_task
from test.models import UserWebsite


@shared_task
def task_add_website_via_rabbit_1():
    print("Adding via rabbit. queue 1")
    UserWebsite.objects.create(url="queue-1.rabbit.com")

@shared_task
def task_add_website_via_rabbit_2():
    print("Adding via rabbit. queue 2")
    UserWebsite.objects.create(url="queue-2.rabbit.com")
