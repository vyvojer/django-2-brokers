from django.db import models

# Create your models here.


class UserWebsite(models.Model):
    url = models.CharField(max_length=100)
    logo = models.TextField(null=True)