import base64
from io import BytesIO

from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile

from django.http import HttpRequest
from django.shortcuts import render, redirect
from django import forms

# Create your views here.
from django.urls import reverse

from test.models import UserWebsite
from test.tasks.redis import task_add_website_via_redis_1, task_add_website_via_redis_2
from test.tasks.rabbit import task_add_website_via_rabbit_1, task_add_website_via_rabbit_2


class UserWebsiteForm(forms.Form):
    url = forms.CharField(max_length=100)
    logo_file = forms.ImageField(required=False, label="Logo on the status page")


def index(request: HttpRequest):
    template_name = "test/index.html"
    if request.method == 'POST':
        form = UserWebsiteForm(request.POST, request.FILES)
        if form.is_valid():
            print('VALID')
            url = form.cleaned_data['url']
            logo_file = form.cleaned_data['logo_file']
            logo = get_logo(logo_file)
            UserWebsite.objects.create(url=url, logo=logo)
            return redirect(reverse("index"))
    else:
        form = UserWebsiteForm()
    websites = UserWebsite.objects.all()
    context = {
        'title': 'websites',
        'websites': websites,
        'form': form,
    }
    return render(request, template_name, context)


def get_logo(logo: InMemoryUploadedFile):
    if not logo:
        return ""

    image_file = BytesIO(logo.read())
    image = Image.open(image_file)
    image.thumbnail((300, 50), Image.ANTIALIAS)

    image_file = BytesIO()
    image.save(image_file, "PNG", quality=90)

    img_str = base64.b64encode(image_file.getvalue()).decode()
    return "data:image/png;base64, " + img_str


def tasks(request: HttpRequest):
    context = {'tasks': UserWebsite.objects.all()}

    return render(request, "test/tasks.html", context)


def add_websites(request: HttpRequest):
    task_add_website_via_redis_1.apply_async(countdown=10)
    task_add_website_via_redis_2.apply_async(countdown=10)
    task_add_website_via_rabbit_1.apply_async(countdown=10)
    task_add_website_via_rabbit_2.apply_async(countdown=10)
    return redirect("tasks")
