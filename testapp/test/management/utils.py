from django.conf import settings


def celery_args() -> dict:
    args = {}
    args["without_gossip"] = settings.CELERY_WITHOUT_GOSSIP
    args["without_mingle"] = settings.CELERY_WITHOUT_MINGLE
    args["without_heartbeat"] = settings.CELERY_WITHOUT_HEARTBEAT
    try:
        args["prefetch_multiplier"] = settings.CELERY_WORKER_PREFETCH_MULTIPLIER
    except AttributeError:
        pass
    try:
        args["optimization"] = settings.CELERY_OPTIMIZATION
    except AttributeError:
        pass
    return args
