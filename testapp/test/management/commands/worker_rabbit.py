from typing import Dict, Iterable, Tuple

from testapp.celery_rabbit import app
from celery.bin.worker import worker
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args: Tuple[str], **options: Dict[str, Iterable]) -> int:
        """
        See:

        https://github.com/celery/celery/blob/master/celery/bin/worker.py
        """
        app.select_queues(["testapp.rabbit.1", "testapp.rabbit.2"])
        return worker(app=app).run()
