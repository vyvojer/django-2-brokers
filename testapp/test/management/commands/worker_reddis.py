from typing import Dict, Iterable, Tuple

from testapp.celery_redis import app
from celery.bin.worker import worker
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args: Tuple[str], **options: Dict[str, Iterable]) -> int:
        """
        See:

        https://github.com/celery/celery/blob/master/celery/bin/worker.py
        """
        app.select_queues(["testapp.redis.1", "testapp.redis.2"])
        return worker(app=app).run()
