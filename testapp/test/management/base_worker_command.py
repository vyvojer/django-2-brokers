import logging
from importlib import reload

import gevent
from django.conf import settings
from django.core.management import BaseCommand
from django.utils.autoreload import code_changed
from gevent.pool import Group

log = logging.getLogger(__name__)


class BaseWorkerCommand(BaseCommand):
    consumer_module = None
    should_reload = True

    def __init__(self):
        super(BaseWorkerCommand, self).__init__()
        self.consumers = []
        self.runners = Group()

    def handle(self, *args, **options):

        try:
            self.start_consumers()

            if settings.DEBUG:
                self.autoreload()

            self.runners.join()
        except KeyboardInterrupt:
            self.stop_consumers()

    def autoreload(self):
        while self.should_reload:
            if code_changed() is not False:
                self.stop_consumers()
                reload(self.consumer_module)
                self.start_consumers()

            gevent.sleep(1)

    def get_consumers(self):
        raise NotImplementedError

    def start_consumers(self):
        log.debug("Starting consumers")
        for consumer in self.get_consumers():
            consumer.runner = self.runners.spawn(consumer.run)
            consumer.runner.link_exception(self.handle_exception)
            self.consumers.append(consumer)
        log.info("Consumers are up and running")

    def stop_consumers(self):
        log.debug("Stopping consumers")
        for consumer in list(self.consumers):
            consumer.consumer_is_running = False
            consumer.should_stop = True
            if consumer.runner:
                consumer.runner.join()
            consumer.stop()
            self.consumers.remove(consumer)
        log.info("Consumers stopped")

    def handle_exception(self, greenlet, **kwargs):
        try:
            greenlet.get()
        except Exception:
            log.exception("Exception in consumer, quitting")
            self.should_reload = False
            self.stop_consumers()
