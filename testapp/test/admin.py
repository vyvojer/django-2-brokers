from django.contrib import admin

# Register your models here.
from test.models import UserWebsite


@admin.register(UserWebsite)
class UserWebsiteAdimin(admin.ModelAdmin):
    list_display = ['url']